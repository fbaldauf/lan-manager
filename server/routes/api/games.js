const express = require('express');
const mongodb = require('mongodb');
const multipart = require('connect-multiparty');
const multiparty = multipart();
const fs = require('fs');

const router = express.Router();

// Get games
router.get('/', async (req, res) => {
  const games = await loadGamesCollection();

  res.send(await games.find({}).toArray());
});

// Add game
router.post('/', multiparty, async (req, res) => {
  const games = await loadGamesCollection();

  if (req.files.file && req.files.file.size > 0) {
    const tmp_path = req.files.file.path;
    const target_path =
      __dirname + '/../../../public/upload/' + req.files.file.name;

    fs.rename(tmp_path, target_path, function(err) {
      if (err) {
        throw err;
      }
      fs.unlink(tmp_path, function() {
        if (err) {
          throw err;
        }
      });
    });
  }

  const cover = (req.files.file && req.files.file.name) || '';

  await games.insertOne({
    text: req.body.text,
    fileshare: req.body.fileshare,
    cover: cover,
    createdAt: new Date(),
  });

  res.status(201).send();
});

// Delete game
router.delete('/:id', async (req, res) => {
  const games = await loadGamesCollection();

  await games.deleteOne({ _id: new mongodb.ObjectID(req.params.id) });

  res.status(200).send();
});

async function loadGamesCollection() {
  const db = await loadDatabase();

  return db.collection('games');
}

async function loadDatabase() {
  const client = await mongodb.MongoClient.connect('mongodb://localhost', {
    useNewUrlParser: true,
  });

  return client.db('lan');
}

module.exports = router;
