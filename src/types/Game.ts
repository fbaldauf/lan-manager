export interface IGame {
  title: string;
  shares: string[];
}
