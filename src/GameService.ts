import axios, { AxiosPromise } from 'axios';
import { IGame } from './types/Game';

const url = 'api/games/';

export class GameService {
  // Get Games
  public static async getGames(): Promise<IGame[]> {
    const res = await axios.get(url);
    const data = res.data;

    return data.map((game: object) => ({
      ...game,
      createdAt: new Date(),
    }));
  }

  // Create Games
  public static insertGame(formData: FormData): Promise<AxiosPromise> {
    // text, fileshare, cover
    return axios.post(url, formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    });
  }

  // Delete Games
  public static deleteGame(id: string): Promise<AxiosPromise> {
    return axios.delete(`${url}${id}`);
  }
}
